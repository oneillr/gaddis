/** 
 * Month is a class that stores a month.
 * This class can get month numbers, month names, and compare Month objects.
 */

public class Month
{
  
   private int monthNumber; // The number of the month. 1 is January, etc.
   
   /**
    * This no-arg constructor creates the object with a default January month.
    */
    
   public Month() 
   {
      monthNumber = 1;
   }
   
   /**
    * This constructor creates the object with a inputted integer for the month.
    * @param m The month as an integer.
    */
   public Month(int m) 
   {
      monthNumber = m;
   }
   
   /**
    * This constructor creates the object with a inputted string for the month,
    * @param str The month as an string.
    */

   public Month(String str) 
   {
      // Create a str2 string that is a lowercase-only copy of the inputted string.
      String str2 = str.toLowerCase();
      // Set the month number based on str2.
      if (str2.equals("january")) 
        monthNumber = 1;
      else if (str2.equals("february")) 
        monthNumber = 2;
      else if (str2.equals("march")) 
        monthNumber = 3;
      else if (str2.equals("april")) 
        monthNumber = 4;
      else if (str2.equals("may")) 
        monthNumber = 5;
      else if (str2.equals("june")) 
        monthNumber = 6;
      else if (str2.equals("july")) 
        monthNumber = 7;
      else if (str2.equals("august")) 
        monthNumber = 8;
      else if (str2.equals("september")) 
        monthNumber = 9;
      else if (str2.equals("october")) 
        monthNumber = 10;
      else if (str2.equals("november")) 
        monthNumber = 11;
      else if (str2.equals("december")) 
        monthNumber = 12;
      else
        monthNumber = 0;
   }

   /**
    * The setMonthNumber method sets a month number based on a passed integer.
    * @param m The month as an integer.
    */
    
   public void setMonthNumber(int m)
   {
      monthNumber = m;
   }
   
   /**
    * The getMonthNumber method returns the month number.
    * @return The month number as an integer.
    */

   public int getMonthNumber()
   {
      return monthNumber;
   }

   /**
    * The getMonthName method returns the month name.
    * @return The month name as a string.
    */
    
   public String getMonthName()
   {
      String str; // Initialize a string object to hold the month name.
      switch (monthNumber) // Set month name based on month number.
      {
          case 1:  str = "January";
                   break;
          case 2:  str = "February";
                   break;
          case 3:  str = "March";
                   break;
          case 4:  str = "April";
                   break;
          case 5:  str = "May";
                   break;
          case 6:  str = "June";
                   break;
          case 7:  str = "July";
                   break;
          case 8:  str = "August";
                   break;
          case 9:  str = "September";
                   break;
          case 10: str = "October";
                   break;
          case 11: str = "November";
                   break;
          case 12: str = "December";
                   break;
          default: str = "Not Valid";
                   break;
      }
      return str; // Return month name.
   }

   /**
    * The toString method returns the month's name.
    * @return The month name as a string.
    */
    
   @Override
   public String toString()
   {
      return this.getMonthName();
   }

   /**
    * The equals method returns if the passed month's month number is the same.
    * @param month The Month object to compare to.
    * @return Comparison of month numbers as a boolean.
    */
    
   public boolean equals(Month month)
   {
      if (monthNumber == month.monthNumber)
         return true;
      else
         return false;
   }
   
   /**
    * The greaterThan method returns if the calling Month is later than the passed Month.
    * @param month The Month object to compare to.
    * @return Comparison of month numbers as a boolean.
    */
    
   public boolean greaterThan(Month month)
   {
      if (monthNumber > month.monthNumber)
         return true;
      else
         return false;
   }

   /**
    * The lessThan method returns if the calling Month is earlier than the passed Month.
    * @param month The Month object to compare to.
    * @return Comparison of month numbers as a boolean.
    */
    
   public boolean lessThan(Month month)
   {
      if (monthNumber < month.monthNumber)
         return true;
      else
         return false;
   }
}