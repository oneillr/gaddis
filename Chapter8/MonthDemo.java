import java.util.Scanner;

/** 
 * MonthDemo is a class that tests the Month class.
 * The user will input a name of a month to create a Month object with.
 * The demo will also create a 'march' and 'july' object to compare with.
 * The demo will then compare the inputted month to 'march' and 'july'. 
 */
 
public class MonthDemo
{
   public static void main(String[] args)
   {
      Scanner keyboard = new Scanner(System.in); // Scanner for input.
      
      // Get user input for month.
      System.out.println("Enter the name of a month to compare to March and July.");
      String inputString = keyboard.nextLine(); 
      Month testMonth = new Month(inputString); // Create Month object based on input.
      
      // Create 'march' and 'july' Month objects to compare to.
      Month march = new Month(3);
      Month july = new Month("July");
      
      // Show user the information for the inputted month's Month object.
      System.out.printf("Inputted month name: %s \n", testMonth.toString());
      System.out.printf("Inputted month number: %d \n\n", testMonth.getMonthNumber());
      
      // If the input was not valid, set month to January for test purposes.
      if (testMonth.getMonthNumber() == 0)
      {
         System.out.println("This is not a valid month. Setting month to January.\n");
         testMonth.setMonthNumber(1);
      }
      
      // Compare the inputted month to 'march'.  
      if (testMonth.equals(march))
         System.out.printf("%s is the same month as %s\n", testMonth.toString(), march.toString());
      else if (testMonth.greaterThan(march))
         System.out.printf("%s is a later month than %s.\n", testMonth.toString(), march.toString());
      else if (testMonth.lessThan(march))
         System.out.printf("%s is a earlier month than %s.\n", testMonth.toString(), march.toString());
      
      // Compare the inputted month to 'july'.
      if (testMonth.equals(july))
         System.out.printf("%s is the same month as %s.\n", testMonth.toString(), july.toString());
      else if (testMonth.greaterThan(july))
         System.out.printf("%s is a later month than %s.\n", testMonth.toString(), july.toString());
      else if (testMonth.lessThan(july))
         System.out.printf("%s is a earlier month than %s.\n", testMonth.toString(), july.toString());
   }
}