import java.util.Scanner;
/** 
 * PrimeNumberList is a class that prints a list of prime numbers in a range.
 * The user inputs the minimum of the range and the maximum of the range;
 * the program then makes a list of prime numbers in that range.
 * This includes auxillary isPrime and isEven methods.
 */
public class PrimeNumberList
{
   public static void main(String[] args)
   {
      // Initialize variables - testNumber is the start number, maxNumber is the limit number.
      int minNumber, maxNumber;
      
      // Initialize a Scanner object for keyboard input.
      Scanner keyboard = new Scanner(System.in);
      
      // Ask for input - what is the range they want to test? If -1 is entered, exit program. If invalid numbers, loop.
      do {
         System.out.print("\nWhat is the first whole positive number you want to test is prime? (-1 to exit) ");
         minNumber = keyboard.nextInt();
         if (minNumber == -1) // Exit?
            System.exit(0);
         System.out.print("\nWhat is the maximum whole number you want to test is prime? (-1 to exit)");
         System.out.print("\nThis must be equal to or higher than " + minNumber + ". ");
         maxNumber = keyboard.nextInt();
         if (maxNumber == -1) // Exit?
            System.exit(0);
         } while (minNumber <= 0 || maxNumber < minNumber);
      
      // Format list.
      System.out.printf("\n\nList of prime numbers from %d to %d:", minNumber, maxNumber);
      System.out.print("\n--------------------------------------\n");
      
      // Loop through the test number list. Start at minNumber and finish with maxNumber.
      for (int testNumber = minNumber; testNumber <= maxNumber; testNumber++) 
      {
         // If it's prime, print it! Else, loop accordingly.
         if (isPrime(testNumber))
            System.out.println(testNumber);
      }
   }
   
   /**
   * The isEven method detemines if an integer is an even number.
   * @param inputNumber The number to test as even.
   */
   
   public static boolean isEven(int inputNumber)
   { 
      // Test the number. If it is disible by 2, then it is even (no remainder).
      if (inputNumber % 2 == 0)
         return true;
      // If it's not even, return false.
      return false;
   }
   /**
   * The isPrime method detemines if an integer is prime.
   * It's not the most efficient implementation.
   * @param inputNumber the number to test if Prime.
   */
   public static boolean isPrime(int inputNumber)
   {
      // if the number 1? It's prime.
      if (inputNumber == 1)
         return true;
      // Is the number even? If so, it's not prime (divisble by 2).
      if (isEven(inputNumber))
         return false;
      /* Test individual numbers for a modulus result of 0.
      *  Begin at 3. Divide the number by testNumber. maxTestNumber will make sure we don't test redundantly.
      *  (if number isn't divisible by 3, it is not divisible by any product of three)
      *  Test by the next odd number (add 2 to testNumber.)
      */ 
      for (int testNumber = 3, maxTestNumber = inputNumber/2; maxTestNumber > testNumber; testNumber += 2) 
      {
         // If the number has no remainder, this is not prime, so return false.
         if (inputNumber % testNumber == 0)
            return false;
         // inputNumber is not divisible by testNumber. Adjust maxTestNumber accordingly and loop as needed.
         maxTestNumber = inputNumber/testNumber;
      }
      // If the loop ends, then this number is Prime.
      return true;
   }
}