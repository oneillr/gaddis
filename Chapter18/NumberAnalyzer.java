import java.util.ArrayList;
import java.util.Arrays;

/**
 * The NumberAnalyzer class is a generic class that accepts
 * an array of Number objects.
 * It can return the maximum, minimum and average of the
 * list of numbers and can also add and remove numbers. 
 */

public class NumberAnalyzer<T extends Number>
{
   private ArrayList<T> numList;
   
   /**
      Constructor
   */
   
   public NumberAnalyzer(T[] array)
   {
     numList = new ArrayList<T>(Arrays.asList(array));
   }
   
   /**
      The maxNum method returns the maximum value
      in the numList.
      @return The maximum value in the numList.
   */
   
   public T maxNum()
   {
      // Get the first value in the numList.
      T maxNum = numList.get(0);
      
      // Iterate through the list and find the maximum
      for (int index = 1; index < numList.size(); index++)
      {
         T testNum = numList.get(index);
         if (testNum.doubleValue() > maxNum.doubleValue())
            maxNum = testNum;
      }
      
      // Return the maximum of numbers.
      return maxNum;
      
   }

   /**
      The minNum method returns the minimum value
      in the numList.
      @return The minimum value in the numList.
   */
   
   public T minNum()
   {
      // Get the first value in the numList.
      T minNum = numList.get(0);
      
      // Iterate through the list and find the mnimum.
      for (int index = 1; index < numList.size(); index++)
      {
         T testNum = numList.get(index);
         if (testNum.doubleValue() < minNum.doubleValue())
            minNum = testNum;
      }
      
      // Return the minimum of numbers.
      return minNum;
   }

   /**
      The avgNum method returns the average
      of numbers in the numList.
      @return the average as a double.
   */

   public double avgNum()
   {
      // Set avgNum to the first in the list.
      // Since we don't know the Number subclass,
      // we'll do additons via toString parsing.
      
      float avgNumSum = 0; 
      String avgNum = numList.get(0).toString();
      avgNumSum += Float.parseFloat(avgNum);
      
      
      // Iterate through the list and add to the average.
      for (int index = 1; index < numList.size(); index++)
      {
        avgNum = numList.get(index).toString();
        avgNumSum += Float.parseFloat(avgNum);
      }
      
      // Return the average of numbers.
      return avgNumSum / numList.size();
   }
   
  /**
      The countNum method shows the count of
      numbers in the numList.
      @return the number count.
  */
   
   public int countNum()
   {
      return numList.size();
   }
   
   /**
      The addNum method adds a value to the 
      end of the numList.
      @param num the number to add.
   */
   
   public void addNum(T num)
   {
      numList.add(num);
   }
   
   /**
      The removeNum method removes the last 
      entry of the value from the numList.
      @param value The value to remove.
   */
   
   public int removeNum(T num)
   {
      for (int index = (numList.size() - 1) ; index >= 0 ; index--)
      {
         T testNum = numList.get(index);
         if (testNum.equals(num))
         {
            numList.remove(index);
            return index;
         }
      }
      return -1;
   }
   
   @Override
   public String toString()
   {
      String stringList = "[ ";
      
      for (int index = 0; index < numList.size(); index++)
      {
         stringList += (numList.get(index) + ", ");
      }
      
      stringList += "]";
      return stringList;  
      } 
}