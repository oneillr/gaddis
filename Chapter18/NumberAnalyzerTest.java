/**
 * The NumberAnalyzerTest class tests the functionality
 * of the NumberAnalyzer class.
 */
 
public class NumberAnalyzerTest
{
   public static void main(String[] args)
   {
      // Let's make a testing Array for NumberAnalyzer.
      Integer testArray[] = { 5, 20, 100, 50, 3, 20, 9 }  ;

      // Let's send the testArray to NumberAnalyzer called testList.
      NumberAnalyzer<Integer> testList = new NumberAnalyzer<Integer>(testArray);
      
      // Print the list and number of elements.
      System.out.println(testList);
      System.out.println("The number count is " + testList.countNum());
      
      // Print the maximum, minimum, and average.
      System.out.println("The maximum of the list is " + testList.maxNum());
      System.out.println("The minimum of the list is " + testList.minNum());
      System.out.println("The average of the list is " + testList.avgNum());
      
      // Add a number to the list and print data.
      testList.addNum(20);
      System.out.println(testList);
      System.out.println("The number count is " + testList.countNum());
      System.out.println("The maximum of the list is " + testList.maxNum());
      System.out.println("The minimum of the list is " + testList.minNum());
      System.out.println("The average of the list is " + testList.avgNum());
      
      // Let's remove 55. The number will not be found so an array element of -1 is returned.
      System.out.println(" Number 55 was removed at array element " + testList.removeNum(55));
      System.out.println(testList);
      
      // Let's remove 20.
      System.out.println(" Number 20 was removed at array element " + testList.removeNum(20));
      System.out.println(testList);
      
      // Let's remove 20 again.
      System.out.println(" Number 20 was removed at array element " + testList.removeNum(20));
      System.out.println(testList);
      
      // Let's remove 20 yet again.
      System.out.println(" Number 20 was removed at array element " + testList.removeNum(20));
      System.out.println(testList);
      
      // Let's add 150 and calculate one more time.
      testList.addNum(150);
      System.out.println(testList);
      System.out.println("The number count is " + testList.countNum());
      System.out.println("The maximum of the list is " + testList.maxNum());
      System.out.println("The minimum of the list is " + testList.minNum());
      System.out.println("The average of the list is " + testList.avgNum());
   }
}