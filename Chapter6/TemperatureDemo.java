import java.util.Scanner;

/** 
 * TemperatureDemo is a class that tests the Temperature class. 
 * We will have the user input a temperature to set to a new instance of Temperature.
 * We will test all the accessor methods (getFahrenheit, getCelcius, getKelvin) by
 * printing out the temperatures converted from the user's input.
 */
 
public class TemperatureDemo
{
   public static void main(String[] args)
   {
      Scanner keyboard = new Scanner(System.in); // Scanner for input.
      
      System.out.println("What is the temperature in Fahrenheit?");
      double inputTemp = keyboard.nextDouble(); 
      Temperature temp = new Temperature(inputTemp); // Create a new Temperature object with input as argument.

      // We will output all conversions using the accessor methods from this instance.
      System.out.printf("The temperature in Fahrenheit is %.2f \n" +
                        "The temperature in Celcius is %.2f \n" +
                        "The temperature in Kelvin is %.2f", temp.getFahrenheit(), temp.getCelcius(), temp.getKelvin());
   }
}