/** 
 * Temperature is a class where one can construct Temperature object instances.
 * The only field used is fTemp which is the Fahrenheit temperature.
 */

public class Temperature
{
   private double fTemp;
   
   /** 
    * The Temperature constructor will create a Temperature object instance.
    * @param temp The temperature in Fahrenheit.
    */
    
   public Temperature(double temp)
   {
      fTemp = temp;
   }
   
   /** 
    * The setFahrenheit mutator sets the object's temperature in Fahrenheit.
    * @param temp The temperature in Fahrenheit.
    */
    
   public void setFahrenheit(double temp)
   {
      fTemp = temp;
   }
   
   /** 
    * The getFahrenheit accessor returns the object's temperature in Fahrenheit.
    * @return The temperature in Fahrenheit.
    */
    
   public double getFahrenheit()
   { 
      return fTemp;
   }
   
   /** 
    * The getCelcius accessor returns the object's temperature in Celcius.
    * @return The temperature in Celcius.
    */
    
   public double getCelcius()
   { 
      return (5.0 / 9.0) * (fTemp - 32.0);
   }
   
   /** 
    * The getKelvin accessor returns the object's temperature in Kelvin.
    * @return The temperature in Kelvin.
    */
    
   public double getKelvin()
   { 
      return ((5.0 / 9.0) * (fTemp - 32.0)) + 273.15;
   }
}