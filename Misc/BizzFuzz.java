import java.util.Scanner;
public class BizzFuzz
/**
 * The BizzFuss class determines divisibility by 3 and gives the appropriate response.
 * If the number is fully divisible by 3, print Bizz.
 * If the number is fully divisible by 5, print Fuzz.
 * If divisible by both 3 and 5, it will print BizzFuzz.
 */
{ 
   public static void main(String[] args)
   {
   Scanner keyboard = new Scanner (System.in);
   System.out.println("Enter the number to test for BizzFuzz.");
   int testNumber = keyboard.nextInt();
   if (testNumber % 3 == 0 && testNumber != 0)
      System.out.print("Bizz");
   if (testNumber % 5 == 0 && testNumber != 0)
      System.out.print("Fuzz");
   
   }
}