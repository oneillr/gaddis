import java.util.Optional;
import javafx.application.Application; 
import javafx.scene.Scene; 
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.GridPane; 
import javafx.stage.Stage;

/**
 * The Checkerboard class is a JavaFX application that requests
 * a checkerboard size from the user and then creates a window
 * matching that size.
 */

public class Checkerboard extends Application 
{
   @Override
   public void start(Stage stage) 
   {
      // Checkerboard size.
      int numSquares = 0;
      
      // Start input loop. Prompt a text input dialog.
      while (numSquares <= 0)
      {
         try 
         {
            // First, ask the user how many squares across.
            TextInputDialog dialog = new TextInputDialog("5");
            dialog.setTitle("Number Input Dialog");
            dialog.setHeaderText("How many squares across on the checkerboard?");
            dialog.setContentText("Enter a valid number of squares: ");
            if (numSquares < 0) // Was there a bad input?
               dialog.setContentText("ERROR! Enter valid number of squares: ");
            // Retrieve the input. If it is invalid, catch exception and set to -1.
            Optional<String> result = dialog.showAndWait();
            numSquares = Integer.parseInt(result.get());
         }
         catch (NumberFormatException e) 
         {
            numSquares = -1;
         }
      }

      // Create a GridPane to store text fields. 
      GridPane gridPane = new GridPane();
      
      // Let's loop through and create and add text fields.
      for (int row = 0; row < numSquares; row++)
      {
         for (int col = 0; col < numSquares; col++)
         {
            // Instantiate a new text field.
            TextField checkerField = new TextField();
            // Make it so it's not interactable.
            checkerField.setEditable(false);
            checkerField.setMouseTransparent(true);
            checkerField.setFocusTraversable(false);
            // Force a square shape and consistent sizing.
            checkerField.setMaxSize(50, 50);
            checkerField.setMinSize(50, 50);
            // If both coordinates are even or both are odd, set black.
            if (row % 2 == col % 2)
               checkerField.setStyle("-fx-background-color: black");
            // Add this text field to the gridPane.
            gridPane.add(checkerField, row, col);
         }
      }

      // Make the grid lines visible so it looks cleaner. 
      gridPane.setGridLinesVisible(true);
      
      // Set the stage and show us that checkerboard!
      stage.setScene(new Scene(gridPane)); 
      stage.setTitle("Checkerboard"); 
      stage.show();
   }
}
      