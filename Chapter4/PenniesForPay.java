import java.util.Scanner;
/** 
 * Pennys for Pay is a class that calculates the final pay over a period of time.
 * For a number of days, calculate the final pay based on if the first day pays
 * one penny, the second day two, the third four, and doubling accordingly.
 * The output is formatted into a table showing progression each day.
 */
public class PenniesForPay
{ 
   public static void main(String[] args)
   {
      // Initialize variables.
      int totalDays;
      double totalValue;
      final double PENNY_VALUE = 0.01; // A penny is always worth one cent USD.
      Scanner keyboard = new Scanner (System.in); // We need a scanner for keyboard input.
      
      // Begin input loop. Only continue if the value of totalDays inputted is 1 or more. If -1, exit program.
      do {
         System.out.println("How many days of Pennies For Pay? Please enter a whole number of 1 or greater. (-1 to exit) ");
         totalDays = keyboard.nextInt(); // Take scanner input as Int.
         if (totalDays == -1) // Exit?
            System.exit(0);
         } while (totalDays < 1);
         
      // Start table formatting.
      System.out.println("Day\t\tTotal (USD)");
      System.out.println("--------------------");
      
      // Start calculation loop. Start on Day 1.
      for (int currentDay = 1; currentDay <= totalDays; currentDay++) 
         {
         // The formula to calculate totalValue is PENNY_VALUE times 2 to the currentDay power minus PENNY_VALUE.
         totalValue = PENNY_VALUE * Math.pow(2,currentDay) - PENNY_VALUE;
         // Print out the result tabbed for fomatting into the table via day nunber and pay in USD.
         System.out.printf("%d\t\t\t$%.2f\n", currentDay, totalValue);
         }
         
      // Lastly, print out the final total.
      totalValue = PENNY_VALUE * Math.pow(2,totalDays) - PENNY_VALUE;
      System.out.printf("Your final pay after %d days is $%.2f.", totalDays, totalValue);
      
      // This can easily be used for other coins (such as a nickle aka 0.05 dollars) if PENNY_VALUE is changed.
   }
}
