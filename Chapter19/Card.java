/**
 * The Card class represents a playing card.
 * Each card has a suit and a value.
 */

public class Card
{
   /**
   *  The CardValue enum holds all card values.
   */
   
   public enum CardValue 
   {
      ACE,TWO,THREE,FOUR,FIVE,SIX,SEVEN,
      EIGHT,NINE,TEN,JACK,QUEEN,KING;
   }
   
   /**
   *  The CardSuit enum holds all card suits.
   */
   
   public enum CardSuit
   {
      CLUBS, DIAMONDS, HEARTS, SPADES;
   }

   // Each card has a value and suit.
   private final CardValue value;
   private final CardSuit suit;
   
   /**
      Constructor
      @param cv The card's value.
      @param cs The card's suit.
   */
   
   public Card(CardValue cv, CardSuit cs)
   {
      value = cv;
      suit = cs;
   }
   
   /**
   *  The getSuit method returns the suit of the card.
   *  return CardSuit The Suit of the card.
   */
   
   public CardSuit getSuit()
   {
      return suit;
   }
   
   /**
   *  The getValue method returns the value of the card.
   *  return Cardvalue The value of the card.
   */
   
   public CardValue getValue()
   {
      return value;
   }
   
   /**
   *  The toString method returns the value and suit of the card
   *  as a Strng.
   *  return String The vaulue and suit of the card.
   */
   
   @Override
   public String toString()
   {
      return String.format("%s of %s", value, suit);
   }
      
}