/**
 * The TestCard class tests the Card, Deck, and Hand objects.
 * A deck will be created, populated, and shuffled.
 * A hand (Player 1) will be made and dealt 5 cards from the deck.
 */

public class TestCard
{
   public static void main(String[] args)
   {
      // Initialize deck and hand.
      Deck testDeck = new Deck();
      Hand Player1 = new Hand();
      
      // Populate the deck with cards.
      testDeck.populateDeck();
      
      // Shuffle the deck with cards.
      testDeck.deckShuffle();
      
      System.out.println("Deck card count: " + testDeck.deckSize());
      System.out.println("Deck contents: \n" + testDeck);
      
      System.out.println("Drawing 5 cards for Player 1.");
      
      // We'll draw 5 cards for Player 1's hand.
      for (int c = 0; c < 5; c++)
      {
         Card drawnCard = testDeck.dealCard();
         Player1.getCard(drawnCard);
         System.out.println("Drawn card: " + drawnCard);
      }
      
      // Show Player 1's hand.
      System.out.println("Player 1's Hand: " + Player1.showCards());
      
      // Show that the deck has shrunk.      
      System.out.println("Deck card count: " + testDeck.deckSize());
      
      // Repopulate the deck and show card count.
      System.out.println("Repopulating deck...");
      testDeck.populateDeck();
      System.out.println("Deck card count: " + testDeck.deckSize());
      
   }
}