import java.util.*;

/**
 * The Deck class holds cards from the Card class.
 * We can populate the deck with Cards for a standard deck.
 * We can also deal cards from this deeck.
 */ 
 
public class Deck
{
   // Make the list of cards for the deck.
   private ArrayList<Card> cardList;
   
   /**
    * Constructor
    */
   
   public Deck(){
      // Create the card list. It will start as empty.
      cardList = new ArrayList<Card>();
   }
   
   /**
    * The populateDeck no-arg constructor gives the cardList
    * a standard deck of 52 cards.
    */
  
   public void populateDeck()
   {
      // If deck is not empty, empty it.
      if (!cardList.isEmpty())
         cardList.clear();
         
      // For each Suit...
      for (Card.CardSuit s: Card.CardSuit.values())
      {
         // For each potential value...
         for (Card.CardValue v: Card.CardValue.values())
         {
            // Add the suit and value as a new card.
            cardList.add(new Card(v, s));
         }
      }
   }
   
   /**
   * The deckSize method gives the amount of cards in the deck.
   * @return int The card count of the deck.
   */
   
   public int deckSize()
   {
      return cardList.size();
   }
      
   /**
   * The deckShuffle method shuffles the cardList.
   */
   
   public void deckShuffle()
   {
      Collections.shuffle(cardList);
   }
   
   /**
   * The dealCard method returns the top card in the deck.
   * @return Card The removed Card object.
   */
   
   public Card dealCard()
   {
      return cardList.remove(0);
   }
   
   /**
   * the toString method returns a String.
   * The String is each card in the Deck separated
   * by a newline character.
   * @return String with all cards in the deck.
   */
   
   @Override
   public String toString()
   {
      String stringList = "";
      // Create an iterator for the cardList.
      Iterator<Card> iter = cardList.iterator();

      while (iter.hasNext())
      {
         // Add the card to the string and separate with a newline.
         stringList += (iter.next() + "\n");             
      }
      
      // Return the string.
      return stringList;
   }
         
      
}