import java.util.*;

/**
*  The Hand class extends the Deck class.
*  The hand is a list of cards just like the Deck.
*  We can show the hand and add a card to the hand.
*/

public class Hand extends Deck
{
   // Make the list of cards for the hand.
   private ArrayList<Card> cardList;
   
   /**
    * Constructor
    */
    
   public Hand()
   {
      // Create the list of cards. It starts as empty.
      cardList = new ArrayList<Card>();
   }
   
   /**
    * The getCard method adds the passed Card to the hand.
    * @param c The Card to add to the hand.
    */
    
   public void getCard(Card c)
   {
      cardList.add(c);
   }
   
   /**
    * The toString command returns the list of cards as a String.
    * @return String the list of cards based on ArrayList toString.
    */
    
   public String showCards()
   {
      return cardList.toString();
   }
}  