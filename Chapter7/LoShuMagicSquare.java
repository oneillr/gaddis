import java.util.Scanner;

/** 
 * LoShuMagicSquare is a class that lets the user input numbers into a 3x3 square array.
 * The entered array is then checked to see if the square is a Lo Shu Magic Square.
 */
 
public class LoShuMagicSquare
{
   public static void main(String[] args)
   {
      // First, let's make the array to test for Lo Shu. We will also create an input number and Scanner.
      int[][] testArray = new int[3][3];
      int input = 1;
      Scanner keyboard = new Scanner(System.in);
      
      // Let's ask for numbers to input via iteration.
      for (int row = 0; row < testArray.length; row++)
      {
         for (int col = 0; col < testArray[row].length; col++)
         {
            do // Input test loop. do is used to ignore previous input.
            {
               if (input < 1 || input > 9) // If previous input was invalid, alert the user.
                  System.out.print("Invalid input. ");
                  
               System.out.println("What whole number from 1 to 9 is in row " + (row+1) + " column " + (col+1) +
                                  "? (type -1 to exit)");
               input = keyboard.nextInt();
               
               if (input == -1) // if -1 is entered, exit the application.
                  System.exit(0);
                  
            } while (input < 1 || input > 9);
            testArray[row][col] = input;
         }
      }
      
      // Print out the square array being tested.
      for (int row = 0; row < testArray.length; row++)
      {
         System.out.println("");
         for (int col = 0; col < testArray[row].length; col++)
         {
            System.out.print(testArray[row][col] + " ");
         }
      }
         
      // Use the isLoShu method to test the array.
      if (isLoShu(testArray))
         System.out.println("\nThis square is a Lo Shu Magic Square.");
      else
         System.out.println("\nThis square is NOT a Lo Shu Magic Square.");
   }
   
   /**
   * The isLoShu method determines if the 3x3 2D array is a Lo Shu Square.
   *
   * @param inputArray the 3x3 array to be tested.
   * @return True or false boolean (True means it IS Lo Shu).
   */
   
   public static boolean isLoShu(int[][] inputArray)
   {
      /* 
         Since the magic square has unique numbers 1-9, make a uniqueArray to compare to.
         (Can I do this another way? Maybe Factorials/Triagular Numbers?)
         We will make an array that holds the sums of rows/columns/diagonals.
         We also initialize the number we are testing.
         We only iterate once through inputArray!
      */
      
      int[] uniqueArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
      int[][] sumArray = new int[3][3]; // [0][0~2] are row sums, [1][0~2] are column sums, [2][0] and [2][1] are diagonal sums.
      int testNum = 0;
      
      for (int row = 0; row < inputArray.length; row++)
      {
         for (int col = 0; col < inputArray[row].length; col++)
         {
            testNum = inputArray[row][col]; // Get the test number.
            
            // Is this unique? Set relevant uniqueArray subscript to 0.
            if (inputArray[row][col] == uniqueArray[testNum-1])
               uniqueArray[(testNum-1)] = 0;
            else // Else, no way this is a Lo Shu.
               return false;
               
            sumArray[0][row] += testNum; // Add to correct row column.
            sumArray[1][col] += testNum; // Add to correct column... column.
            
            // Check for diagonal coordinates [top left to bottom right]
            if (row == col)
               sumArray[2][0] += testNum;
            // Check for the other diagonals [bottom left to top right]
            if (row + col == 2)
               sumArray[2][1] += testNum;
         }
      }
      // At this point, we know all numbers are 1-9 and unique. We summed them in an array.
      // Are all the sums the same? Set testnum to first sum.
      // If we're checking row 2 col 2, then we know it's Lo Shu.
      testNum = sumArray[0][0];
      for (int row = 0; row < sumArray.length; row++)
      {
         for (int col = 0; col < sumArray[row].length; col++)
         {
            if (row == 2 && col == 2)
               return true;
            if (sumArray[row][col] != testNum)
               return false;
         }
      }
      
      System.out.println("Something happened."); // If this happens, then something happened.
      return false;
   }
}