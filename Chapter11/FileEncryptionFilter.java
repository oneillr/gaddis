import java.io.*;
import javax.swing.*;    
import java.awt.event.*;

/** 
 * FileEncryptionFilter is a GUI that encrypts and decrypts files.
 * The Swing GUI has the user type in an input filename and output filename.
 * The user will click decrypt or encrypt with their choice.
 * The appropriate action will occur. A dialog box will return success or exception.
 */
 
public class FileEncryptionFilter extends JFrame
{      
   private JPanel panel;                  
   private JLabel inputLabel;             // Input Filename label
   private JTextField inputFile;          // Input Filename
   private JLabel outputLabel;            // Output Filename label
   private JTextField outputFile;         // Output Filename
   private JButton encryptButton;         // Encrypt button
   private JButton decryptButton;         // Decrypt button
   private String inputFilename, outputFilename;   // Strings to pass to methods.
   
   /**
    * Constructor
    */
   
   public FileEncryptionFilter()
   {
      // Initialize the window's settings.
      setTitle("File Encryption Filter");
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

      //Create the buttons, labels, and text fields.
      decryptButton = new JButton("Decrypt File");
      encryptButton = new JButton("Encrypt File");
      inputLabel = new JLabel("Input filename: ");
      outputLabel = new JLabel("Output filename: ");
      inputFile = new JTextField(20);
      outputFile = new JTextField(20);
      
      // Register an event listener for both buttons.
      decryptButton.addActionListener(new ButtonListener());
      encryptButton.addActionListener(new ButtonListener());

      // Create a panel and add the objects to it.
      panel = new JPanel();
      panel.add(inputLabel);
      panel.add(inputFile);
      panel.add(outputLabel);
      panel.add(outputFile);
      panel.add(encryptButton);
      panel.add(decryptButton);
      add(panel);
      pack(); // Pack it!
      
      // Show this to the user.
      setVisible(true);
   }
   
   /**
    * ButtonListener is an action listener class for
    * the encrypt and decrypt buttons.
    */
   
   private class ButtonListener implements ActionListener
   {
      public void actionPerformed(ActionEvent e)
      {
         // Set inputFilename and outputFilename based on text fields.
         inputFilename = inputFile.getText();
         outputFilename = outputFile.getText();
         
         // Determine which button was clicked.
         // Initialize the correct method accordingly.
         if (e.getSource() == encryptButton)
         {
            // Start encryption. Show a message dialog with the returned string.
            JOptionPane.showMessageDialog(null, encryptFile(inputFilename, outputFilename));
         }
         else if (e.getSource() == decryptButton)
         {
            // Start decryption. Show a message dialog with the returned string.
            JOptionPane.showMessageDialog(null, decryptFile(inputFilename, outputFilename));
         }
      }
   }
   
   /**
    * The encryptFile method accepts an input and output file name.
    * The file will be encrypted (+10 to Unicode per character).
    * The method returns a String with either success or exception.
    * @param inputFilename The filename of the input file.
    * @param outputFilename the filename of the output file.
    * @return A string with success or exception.
    */
    
   public static String encryptFile(String inputFilename, String outputFilename)
   {
      char currentChar;   // The current character.
      int charInt;        // A character in integer.
      
      try
      {
         // The files need to be different. Throw an exception if not.
         if (inputFilename.equals(outputFilename))
            throw new IOException("Filenames must be different.");
       
         // Open the file
         FileReader fileReader = new FileReader(inputFilename);
         FileWriter fileWriter = new FileWriter(outputFilename);
         
         charInt = fileReader.read();
         while (charInt != -1)
         {
            // Convert the code to a character for debug.
            currentChar = (char) charInt;
            // Print the character for debug.
            System.out.println("Char: " + currentChar);
            // Convert a character to Unicode.
            charInt += 10;
            // Write the Unicode.
            fileWriter.write(charInt);
            // Get the next code from the stream.
            charInt = fileReader.read();
         }
         
         // Close the file.
         fileWriter.close();
         fileReader.close();
      }
      catch (IOException e)
      {
         // Return the error.
         return ("IO Error: " + e.getMessage());
      }
      
      // Return a success.
      return ("Successfully encrypted " + inputFilename + "!");
   }
   
   /**
    * The decryptFile method accepts an input and output file name.
    * The file will be decrypted (-10 to Unicode per character).
    * The method returns a String with either success or exception.
    * @param inputFilename The filename of the input file.
    * @param outputFilename the filename of the output file.
    * @return A string with success or exception.
    */
    
   public static String decryptFile(String inputFilename, String outputFilename)
   {
      char currentChar;   // Current character.
      int charInt;        // Current character in integer.
      
      try
      {
         // Construct both file objects.
         FileReader fileReader = new FileReader(inputFilename);
         FileWriter fileWriter = new FileWriter(outputFilename);
         
         charInt = fileReader.read();
         while (charInt != -1)
         {
            // Convert a character to Unicode.
            charInt -= 10;
             // Convert the code to a character for debug.
            currentChar = (char) charInt;
            // Print the character for debug.
            System.out.println("Char: " + currentChar);
            // Write the Unicode.
            fileWriter.write(charInt);
            // Get the next code from the stream.
            charInt = fileReader.read();
         }
         
         // Close the files.
         fileWriter.close();
         fileReader.close();
      }
      catch (IOException e)
      {
         // Return the error.
         return ("IO Error: " + e.getMessage());
      }
      
      // Return a success.
      return ("Successfully decrypted " + inputFilename + "!");
   }
   
   /**
    * The main class initializes the GUI constructor.
    */
    
   public static void main(String[] args)
   {
      new FileEncryptionFilter();
   }
}