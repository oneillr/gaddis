import java.util.Scanner;

/** 
 * CharacterCountDemo is a class that tests the CharacterCount class.
 * The user will input a test string to get character counts out of.
 * The user will be sent to an interactive menu.
 * The user can get counts of numbers, consonants, and vowels.
 * The user can either input a new string if desired or exit the program. 
 */
 
public class CharacterCountDemo
{
   public static void main(String[] args)
   {
      Scanner keyboard = new Scanner(System.in); // Scanner for input.
      
      // First, get the initial test phrase.
      System.out.println("Type in a phrase to check character counts of.");
      String inputString = keyboard.nextLine();
      CharacterCount testPhrase = new CharacterCount(inputString); // Create a CharacterCount object based on input.
      char inputChar = 'a'; // Initalize menu choice character.
      System.out.printf("\nYour test phrase is \"%s\"", testPhrase.getPhrase()); // Print test string.
       
      do // This will loop...
      {
         // Display the interactive menu.
         System.out.println("\n\nPlease enter your preferred option with the appropriate letter:");
         System.out.println("a. Count the number of vowels in the string");
         System.out.println("b. Count the number of consonants in the string.");
         System.out.println("c. Count both the vowels and consonants in the string.");
         System.out.println("d. Count the number of numbers in the string.");
         System.out.println("e. Input a new string.");
         System.out.println("f. Finish and exit the program.");
         
         // Ask for a character to input. Take the first character, make it lowercase, and store it.
         inputString = keyboard.nextLine();
         inputChar = Character.toLowerCase(inputString.charAt(0));
         
         // Switch cases based on the input character.
         switch (inputChar)
         {
            case 'a': System.out.printf("\nVowels: %d ", testPhrase.getVowelCount());
                      break; // Get vowel count.
                      
            case 'b': System.out.printf("\nConsonants: %d ", testPhrase.getConsonantCount());
                      break; // Get consonant count.
                      
            case 'c': System.out.printf("\nVowels: %d, Consonants: %d", testPhrase.getVowelCount(), testPhrase.getConsonantCount());
                      break; // Get both vowel and consonant count.
                      
            case 'd': System.out.printf("\nNumbers: %d ", testPhrase.getNumberCount());
                      break; // Get number count.
                      
            case 'e': System.out.println("Type in a phrase to check character counts of.");
                      inputString = keyboard.nextLine();
                      testPhrase.setPhrase(inputString);
                      System.out.printf("Your new test phrase is \"%s\"", testPhrase.getPhrase());
                      break; // Request a new string to test.
                      
            case 'f': System.out.println("\nGoodbye!"); 
                      break; // A touching farewell...  
                              
            default : System.out.print("\nInvalid input."); 
                      break; // Any other case is invalid input.
         }
            
      } while (inputChar != 'f'); // ... and it loops until 'f' or 'F' is inputted
      
   }
}