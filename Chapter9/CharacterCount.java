/** 
 * CharacterCount is a class that stores a String to test with.
 * Counts of consonants, vowels, and number digits in the String can be obtained.
 */

public class CharacterCount
{
   private String testPhrase; // The CharacterCount object's phrase.
   
   /**
    * This constructor initializes an object with the passed string.
    * @param str Passed string for testing
    */
    
   public CharacterCount(String str)
   {
      testPhrase = str;
   }
   
   /**
    * The setPhrase method changes the object's phrase to test.
    * @param str Passed string for testing.
    */
    
   public void setPhrase(String str)
   {
      testPhrase = str;
   }
   
   /**
    * The getPhrase method returns the phrase for testing.
    * @return The phrase for testing.
    */
    
   public String getPhrase()
   {
      return testPhrase;
   }
   
   /**
    * The getVowelCount method calculates the number of vowel characters
    * in the test phrase of the CharacterCount object.
    * @return The amount of vowel characters.
    */
   
   public int getVowelCount()
   {
      // Create an all-lowercase char array variant of the phrase we are testing.
      String str = testPhrase.toLowerCase();
      char[] phraseArray = str.toCharArray();
      int vowelCount = 0; // Initialize vowel counter.
      
      // Iterate through the phraseArray to test each individual character for vowel status.
      for (int index = 0; index < phraseArray.length; index++)
      {
         if (phraseArray[index] == 'a' || phraseArray[index] == 'e' || phraseArray[index] == 'i' || phraseArray[index] == 'o' || phraseArray[index] == 'u')
            vowelCount++; // A vowel? Increment count.
      }
      
      return vowelCount; // Return vowel count.
   }
   
   /**
    * The getConsonantCount method calculates the number of consonant characters
    * in the test phrase of the CharacterCount object.
    * @return The amount of consonant characters.
    */

   public int getConsonantCount()
   {
      // Create an all-lowercase char array variant of the phrase we are testing.
      String str = testPhrase.toLowerCase();
      char[] phraseArray = str.toCharArray();
      int consonantCount = 0; // Initialize consonant counter.
      
      // Iterate through the phraseArray to test each individual character for letter status.
      // If it is a letter, confirm that it is NOT a vowel.
      for (int index = 0; index < phraseArray.length; index++)
      {
         if (Character.isLetter(phraseArray[index]))
         {
            // This is a letter? Confirm it's a NOT a vowel.
            if (phraseArray[index] != 'a' && phraseArray[index] != 'e' && phraseArray[index] != 'i' && phraseArray[index] != 'o' && phraseArray[index] != 'u')
               consonantCount++; // Not a vowel? Increment.
         }
      }
      
      return consonantCount; // return consonant count.
   }
  
  /**
    * The getNumberCount method calculates the number of number characters
    * in the test phrase of the CharacterCount object.
    * @return The amount of number characters.
    */

  public int getNumberCount()
   {
      // Create an all-lowercase char array variant of the phrase we are testing.
      char[] phraseArray = testPhrase.toCharArray();
      int numberCount = 0; // Initialize number counter.
      
      // Iterate through the phraseArray to test each individual character for digit status.
      for (int index = 0; index < phraseArray.length; index++)
      {
         if (Character.isDigit(phraseArray[index]))
            numberCount++; // It IS a digit? Increment.
      }
      
      return numberCount; // return number count.
   }
}