/** 
 * Payroll determines the value of one paycheck accoring to the
 * length of hours in the payroll, the pay rate per hour, and net
 * pay after removing taxes according to tax rate.
 */
import java.util.Scanner;
import javax.swing.JOptionPane;

public class Payroll
{ 
  public static void main(String[] args)
  {
    /**
     * It begins.
     */
    int hours;
    double grossPay, netPay, payRate;
    String yourName;
    final double TAX_RATE = 0.85; // You can't avoid taxes - a final constant.
    
    // Collect data. Input dialog outputs String; we need to parse the correct data type.
    hours = Integer.parseInt(JOptionPane.showInputDialog("Enter your hours per pay period."));
    payRate = Double.parseDouble(JOptionPane.showInputDialog("Enter your pay rate per hour."));
    yourName = JOptionPane.showInputDialog("What is your first name?");
    
    // Scanner keyboard = new Scanner(System.in); // We need this for framebuffer input.
    
    /* Deprecated code below (Command line interface):
    System.out.println("Enter your hours per pay period."); // Collect data.
    hours = keyboard.nextInt();
    System.out.println("Enter your pay rate per hour.");
    payRate = keyboard.nextDouble();
    keyboard.nextLine(); // nextLine does not skip newline characters... 
    System.out.println("What is your first name?");
    yourName = keyboard.nextLine();
    */
    
    grossPay = hours * payRate; // Caclulate gross pay.
    netPay = grossPay * (1 - TAX_RATE); // Calculate net pay according to gross.
    
    System.out.println("Hello, " + yourName + "!"); // Print out the results!
    System.out.println("Your gross pay is $" + String.format("%.2f", grossPay));
    System.out.println("This is earned in " + hours + " hours.");
    System.out.println("Your net pay is $" + String.format("%.2f", netPay) + 
                       " after paying " + (TAX_RATE * 100) +
                       "% in taxes. Welcome to the State!");
    JOptionPane.showMessageDialog(null, "Hello, " + yourName + "!\n" +
                                  "Your gross pay is $" + String.format("%.2f", grossPay) + ".\n" +
                                  "This is earned in " + hours + " hours. \n" +
                                  "Your net pay is $" + String.format("%.2f", netPay) + 
                                  " after paying " + (TAX_RATE * 100) +
                                  "% in taxes. Welcome to the State!");
  }
}