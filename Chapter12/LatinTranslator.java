import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/** 
 * LatinTranslator is a GUI shows a list of translated words.
 * The Swing GUI has the user select one Latin word in the word list.
 * The user will click the Translate button to translate to English.
 */
 
 public class LatinTranslator extends JFrame
{      
   private JPanel panel; // Initialize panels
   private JButton translateButton; // Translation button.
   private JLabel translationLabel; // Translation.
   private JList latinList; // List of latin words.
   private String[] englishWords = {"Up", "Down", "Left", "Right", "Center"};
   private String[] latinWords = {"Sursum", "Deorsum", "Sinister", "Dexter", "Medium"};
   
   /**
    * Constructor
    */
   
   public LatinTranslator()
   {
      // Initialize the window's settings.
      setTitle("Latin Translator");
      setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      
      // Build panel.
      buildPanel();

      // Add the main panel.
      add(panel, BorderLayout.CENTER);
      pack(); // Pack it!
      
      // Show this to the user.
      setVisible(true);
   }
   
   /**
    * buildPanel builds the main panel.
    */
    
      private void buildPanel()
   {
      // Construct the panel.
      panel = new JPanel(new BorderLayout());
      
      // Set up the latin word list.
      latinList = new JList(latinWords);
      latinList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      latinList.setSelectedIndex(0); // Give a default.
      
      // Set up the translation button.
      translateButton = new JButton("Translate");
      translateButton.addActionListener(new ButtonListener());
      
      // Set up the translation label/
      translationLabel = new JLabel("Choose a word to translate.");
      
      // Set up the panel.
      panel.add(latinList, BorderLayout.NORTH);
      panel.add(translateButton, BorderLayout.CENTER);
      panel.add(translationLabel, BorderLayout.SOUTH);
   }

   /**
    * ButtonListener is an action listener class for
    * the encrypt and decrypt buttons.
    */
    
   private class ButtonListener implements ActionListener
   {
      public void actionPerformed(ActionEvent e)
      {
         // Try showing the English word at the equivalent array index.
         // This will catch an out of bounds error if necessary
         // if nothing is selected.
         
         try
         {
            String selection = englishWords[latinList.getSelectedIndex()];
            // Set the label to the English word with the same index.
            translationLabel.setText("English: " + selection);
         }
         catch (ArrayIndexOutOfBoundsException ex)
         {
            translationLabel.setText("Choose a word to translate.");
         }
      }
   }
    
    /**
    * The main class initializes the GUI constructor.
    */
    
   public static void main(String[] args)
   {
      new LatinTranslator();
   }
}