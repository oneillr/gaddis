/**
 * The TestAckermann class tests the Ackermann function.
 * This will pass a set of integers into the ackermann()
 * function to demonstrate it.
 */
public class TestAckermann
{
   public static void main(String[] args)
   {
      int m, n; // Integer arguments.
      // Let's make an array of test cases.
      int[][] testArray = new int[][]  {
         {0,0}, {0,1}, {1,1}, {1,2},
         {1,3}, {2,2}, {3,2}, {3,9},
         {3,10}, {3,11}, {3,12}, {3,13},
         {4,0}, {4,1}                  };
      
      // Loop through each test case.
      for (int row = 0; row < testArray.length; row++)
      {
         try
         {
            m = testArray[row][0];
            n = testArray[row][1];
            // Print out results.
            System.out.printf("\nAckermann A(%d,%d) is ", m, n);
            System.out.print(Ackermann.ackermann(m,n));
         }
         // Catch a stack overthrow error if it's too much.
         catch (StackOverflowError e) 
         {
            System.out.print("... a Stack Overflow error. Too much recursion.");
         }
      }
   }
}