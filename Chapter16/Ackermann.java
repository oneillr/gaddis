/* 
 * The Ackermann function is an early example of a function
 * that cannot be a primitive recursive function but is
 * still computable via recursion.
 * Addition is 1 for-cycle, multiplication 2, expotential 3,
 * tetration 4, etc; this number grows in digits 
 * faster than all of those and also cannot be simplified 
 * to equivalent for-cycles.
 */
 
public class Ackermann
{
   /**
    * The ackermann method returns the result
    * of the relevant function based on two inputs.
    * @param m An integer.
    * @param b An integer.
    * @return A integer calculated by the function.
    */
    
   public static int ackermann(int m, int n)
   {
      if (m == 0)
         return n + 1;
      else if (n == 0)
         return ackermann(m - 1, 1);
      else
         return ackermann(m - 1, ackermann(m, n - 1));
   }
}
      