import javax.swing.JOptionPane;
/**
 * The CookieCalories class will determine the user's Cookie Calorie Intake™. 
 * It will request the amount of cookie eaten, the amount of calories they want max,
 * and give a message stating if they are over, under, or matching the cookie difference!
 */
public class CookieCalories
{ 
  public static void main(String[] args)
  {
  int cookiesInBag = 40, servingsPerBag = 10, caloriesPerServing = 300, cookiesEaten, caloriesMaxDesired, cookiesDifference;
  double caloriesEaten, caloriesPerCookie = ((10*300)/40), caloriesDifference; // Declaring and calculating initial variables.
  String appendReccomendation = ""; // Blank string. This will fill in the second part of a dialog.
  
  // Let's get data from the user for further recommendations.
  cookiesEaten = Integer.parseInt(JOptionPane.showInputDialog("Welcome to the Cookie Calorie Intake™ Calculator! \n" +
                                                              "How many cookies did you eat?"));
  caloriesMaxDesired = Integer.parseInt(JOptionPane.showInputDialog("What is your max Cookie Calorie Intake™?"));
  caloriesEaten = caloriesPerCookie * cookiesEaten;
  // Calculate the calorie differences and find out how many cookies that is.
  caloriesDifference = caloriesMaxDesired - caloriesEaten;
  cookiesDifference = (int)(caloriesDifference / caloriesPerCookie);
  System.out.println("Calories difference: " + caloriesDifference + ", Cookies difference: " + cookiesDifference);
  // Let's get the recommendation. appendReccomendation will create the second part of a dialog with results.
  // First, if you clearly ate too many cookies (I cannot fathom there's such a concept...)
  if (caloriesMaxDesired < caloriesEaten)
     if ((cookiesDifference * caloriesPerCookie) % caloriesDifference != 0 || cookiesDifference == 0)
        // This makes sure if the caloriesDifference is -75, -150, etc. that it reccommends the right amount.
        // Modulus of 0 anything is zero, so we adjust for this to avoid a specific case (0 cookies but still over).
        cookiesDifference -= 1;
     appendReccomendation = ("You are over your calorie max (" + caloriesMaxDesired + ")!\n" +
                             "If you eat " + Math.abs(cookiesDifference) + " less cookies, you will eat" +
                             " only " + (caloriesEaten - (caloriesPerCookie * Math.abs(cookiesDifference))) +
                             " calories; \nthis satisfies your limit!");
  // You have room for more Cookie Calories! C'mon, step it up!
  if (caloriesMaxDesired >= caloriesEaten && cookiesDifference >= 1)
     appendReccomendation = ("You are under your calorie max (" + caloriesMaxDesired + ")!\n" +
                             "If you eat " + cookiesDifference + " more cookies, you will eat" +
                             " only " + (caloriesEaten + (caloriesPerCookie * cookiesDifference)) + 
                             " calories;\nthis satisfies your limit!");
  // Actually, looking back, did you eat just the right amount? Readjust the appendReccomendation.
  if (caloriesDifference >= 0 && caloriesDifference < caloriesPerCookie)
     appendReccomendation = ("Wow, you ate exactly the right amount of cookies (" + cookiesEaten + 
                             ")!\nKeep it up!");
  // Print out totals and the reccomendation.
  JOptionPane.showMessageDialog(null, "You ate " + cookiesEaten + " cookies, which mean you ate " + 
                                caloriesEaten + " calories.\n" + appendReccomendation);
  }
}