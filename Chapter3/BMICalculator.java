import javax.swing.JOptionPane;
/**
 * The BMICalculator class will determine the user's BMI. 
 * They will then be told if they are overweight, underweight, or healthy weight. 
 * The program will ask to enter height in weight in either Metric or Imperial.
 * They will also be told the approriate cookie eating habit.
 */
public class BMICalculator
{ 
   public static void main(String[] args)
   {
      double bmi, height, weight;
      int multiplier = 703;
      
      // First, lets determine if the user will require Imperial or Metric.
      Object stringArray[] = { "Imperial", "Metric" }; // Generate options for a dialog.
      int metric = JOptionPane.showOptionDialog(null, "Will you be using Imperial or Metric?", "Imperial or Metric",
         JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, stringArray, stringArray[0]);
      // If metric was chosen, request the correct height and weight units and change multiplier.
      if (metric == 1) {
         multiplier = 10000;
         height = Double.parseDouble(JOptionPane.showInputDialog("Enter your height in centimeters."));
         weight = Double.parseDouble(JOptionPane.showInputDialog("Enter your weight in kilograms."));
      // Otherwise, request Imperial units.
      } else {
         height = Double.parseDouble(JOptionPane.showInputDialog("Enter your height in inches."));
         weight = Double.parseDouble(JOptionPane.showInputDialog("Enter your weight in pounds."));
      }
      // Let's not divide by 0.
      if (height == 0 || weight == 0) {
         JOptionPane.showMessageDialog(null, "Invalid values. Please try again.");
         System.exit(0);
      }
      // Calculate the BMI.
      bmi = (weight / Math.pow(height, 2)) * multiplier;
      // Are you underweight or overweight? If not, your BMI is acceptable.
      // Give reccommendation on how to change their cookie habit. Print results.
      if (bmi > 25){ // Overweight.
         JOptionPane.showMessageDialog(null, "Your BMI is " + String.format("%.2f", bmi) + ".\n" +
                                       "As your BMI is above 25, you are overweight. \n" +
                                       "You might want to stop eating so many cookies.");
      } else if (bmi < 18.5) { // Underweight.
         JOptionPane.showMessageDialog(null, "Your BMI is " + String.format("%.2f", bmi) + ".\n" +
                                       "As your BMI is under 18.5, you are underweight. \n" +
                                       "I would suggest eating many more cookies.");
      } else { // Remainders are normal!
         JOptionPane.showMessageDialog(null, "Your BMI is " + String.format("%.2f", bmi) + ".\n" +
                                       "As your BMI is between 25 and 18.5, you are at a healthy weight. \n" +
                                       "An extra cookie every now and then won't hurt, maybe...");          
      }
   }
}